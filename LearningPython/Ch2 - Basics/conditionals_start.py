#
# Example file for working with conditional statements
# LinkedIn Learning Python course by Joe Marini
#



def main():
    x, y = 1000, 1000

    # conditional flow uses if, elif, else
    if x < y:
        result = "x is less that y"
    elif x == y:
        result = "x is equal to y"
    else:
        result = "x is greater than y"

    print(result)

    # conditional statements let you use "a if C else b"
    result = "X is less than Y " if x<y else "X is greater or equal to Y"
    print(result)

    # match-case makes it easy to compare multiple values - Requires Python 3.10
    # value = "one"
    # match value:
    #     case "one":
    #       result = 1
    #     case_:
    #       result = "default"

if __name__ == "__main__":
    main()
