import re

def is_palindrome(teststr):
    if(len(teststr) <= 1):
       return False

    # Remove the non Ascii Characters
    # Lower the upper case characters
    # Alternativly iterate over the string and check with isalnum
    _str = re.sub(r'\W','',teststr).lower()


    # Reverse the String
    # how does the string actually works in python?
    # for i in range(0,len(_str) ):
    #    _rstr += _str[len(_str) -1 - i]
    _rstr = _str[::-1]

    # Compare and exit
    if(_str == _rstr):
       return True
    return False

if __name__ == "__main__":
  # This is how your code will be called.
  # Your function should return whether a string is a palindrome.
  # The code will count the number of correct answers.
  total = 0
  test_words = ["Hello World!","Radar","Mama?","Madam, I'm Adam.",
      "Race car!"]
  for word in test_words:
      total += is_palindrome(word)

  print(total)
