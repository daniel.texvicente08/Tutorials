#
# Example file for working with classes
# LinkedIn Learning Python course by Joe Marini
#

class Vehicle():
  # The init method is the function that Python calls
  #  when the object has been created and it's time to
  #  initialize the object's data
  # By convention the all of the methods in a class takes
  # the reference to the object as the first parameter
  def __init__(self, bodystyle):
    # define a property on the class
    self.bodystyle = bodystyle

  def drive(self, speed):
    self.mode = "driving"
    self.speed = speed

# Class Car that derives from Vehicle
class Car(Vehicle):
  def __init__(self, enginetype):
    #initialize the superclass, and with the bodystyle
    super().__init__("Car")
    self.wheels = 4
    self.doors = 4
    self.enginetype = enginetype

  def drive(self, speed):
    super().drive(speed)
    print("Driving my", self.enfinetype, "car at", self.speed)

class Motorcycle(Vehicle):
  def __init__(self, enginetype, hassidecar):
    super().__init__("Motorcycle")
    if(hassidecar):
      self.wheels = 3
    else:
      self.wheels = 1

  def drive(self, speed):
      super.drive(speed)
      print("Driving my", self.enfinetype, "motorcycle at", self.speed)

car1 = Car("gas")
car2 = Car("electric")
mc1 = Motorcycle("gas", True)

print( mc1.wheels )
print( car1.enginetype )
print( car2.doors )

car1.drive(30)
car2.drive(40)
mc1.drive(50)
