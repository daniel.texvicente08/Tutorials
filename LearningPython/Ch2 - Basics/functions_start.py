#
# Example file for working with functions
# LinkedIn Learning Python course by Joe Marini
#


# TODO: define a basic function
## def keyword + name + column  (python uses column and whitespace identation)
def func1():
  print("I am a function")
# TODO: function that takes arguments
def func2(arg1, arg2):
  print(arg1," ", arg2)

# TODO: function that returns a value
def cube(x):
  return x * x *x

# TODO: function with default value for an argument
def power(num, x=1):
  result = 1
  for i in range(x):
    result = result * num
  return result


# TODO: function with variable number of arguments
def multi_add(*args):
  result = args[0]
  for x in args[1::]:
    result = result + x
  return result

# func1() # call function directly
# print(func1()) # print 1 - calling the func1 print2 prints None as it returs nothing
# print(func1) # print the return value of the function definition itself

# func2(10, 20)
# print(func2(10, 20))

# print(cube(3))

# print(power(2))
# print(power(2,3))

# # we can change the order of the arguments (must state the variable as declared in the function)
# print(power(x=3, num=2))

print( multi_add(4,5,10,4) )
print( multi_add("one"," amazing", " trick") )