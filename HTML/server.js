const express = require('express');

const app = express();
const port = 8080;

app.use(express.static(__dirname + "/src"));
//app.use(express.static(__dirname + "/renta-agreement-page"));

// sendFile will go here
app.get('/', function(req, res) {
  res.sendFile(__dirname +'/src/index.html');
});

app.get('/2', function(req, res) {
  res.sendFile(__dirname +'/src/page2.html');
});

app.get('/rental', function(req, res) {
  res.sendFile(__dirname + '/renta-agreement-page/index.html');
});

app.listen(port);

console.log(`Server Running at http://localhost:${port}`)
